using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public List<AudioSource> sources;

    public static AudioManager instance;

    /*    public bool isPlaying { get; set; }*/
    private bool isPlaying = false;
    private int counter = 0;
    Coroutine audioPlayer;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;

    }

    // Update is called once per frame
    void Update()
    {
        if (isPlaying)
        {
            //playPlayerMusic();
        }
        else {
        }
    }

    public void playPlayerMusic()
    {
        isPlaying = false;
        foreach (var audioSource in sources)
        {
            if (audioSource.clip != null)
            {
                audioSource.Play();
                
            }
            else
            {
            }
        }

    }

    private IEnumerator Countdown2()
    {
        while (true)
        {
            isPlaying = false;
            foreach (var audioSource in sources)
            {
                if (audioSource.clip != null)
                {
                    audioSource.Play();

                }
                yield return new WaitForSeconds(1);

            }
        }
    }

    public void setIsPlaying()
    {
        Debug.Log(counter);
        counter++;
        if (counter % 2 == 1)
        {
            isPlaying = true;
            audioPlayer = StartCoroutine(Countdown2());

        }
        else { isPlaying = false; 
        
            StopCoroutine(audioPlayer);
        }
    }
}
