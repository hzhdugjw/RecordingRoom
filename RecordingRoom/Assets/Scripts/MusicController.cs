using System.Collections;
using UnityEngine;


public class MusicController : MonoBehaviour
{
    public AudioSource[]audios;
    private  bool isOpen=false;
    Transform audioAreaTransfrom; 
    Transform audioPeerTransform;   
    GameObject audioPeer; 
    AudioSource audioPeerAudioSource;

    private void Start()
    {
        audios = new AudioSource[3];
        audioAreaTransfrom = gameObject.transform;
        audioPeerTransform = audioAreaTransfrom.Find("AudioPeer");
        if (audioPeerTransform != null) { 
        audioPeer = audioPeerTransform.gameObject;
            Debug.Log("audio1" + audioPeer.name);
        audioPeerAudioSource = audioPeer.GetComponent<AudioSource>();
        }

    }

    private IEnumerator PlayMusic()
    {
            
            int i;
            for (i = 0; i <= audios.Length; i++)
            {
            if (i == audios.Length) {
                i = 0;
            }
            if(isOpen==false)
                yield break;
            if (audios[i] != null)
            {

                audioPeerAudioSource.clip = audios[i].clip;
                audioPeerAudioSource.volume = audios[i].volume;
                audioPeerAudioSource.PlayOneShot(audioPeerAudioSource.clip, audioPeerAudioSource.volume);
                Debug.Log(gameObject.name + "-audio clip" + (i + 1) + ":" + audioPeerAudioSource.clip);

            }
            yield return new WaitForSeconds(1);

        }
        
        
    }

    public void OpenMusic() {
        isOpen = true;
        StartCoroutine(PlayMusic());
    }

    public void StopMusic() {
        Debug.Log("进入到停止音乐了");
        isOpen = false;
    }

}
