using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class ButtonPressed : MonoBehaviour
{
    public UnityEvent onPressed, onReleased;
    //[SerializeField] private float threshold = 0.5f;
   
    private bool isPressed;
    private Vector3 startPos;
    private ConfigurableJoint joint;


    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.localPosition; 
        joint = GetComponent<ConfigurableJoint>();

        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(GetValue());
        
       
        if (!isPressed && GetValue() >= 0.5f)
        {
            Pressed();
           
        }

        if (isPressed && GetValue() < 0.1f && GetValue() >= 0)
        {
            Released();
            //Debug.Log(GetValue() - threshold); 
        }
    }

    private float GetValue()
    {
        var value = Vector3.Distance(startPos, transform.localPosition) / joint.linearLimit.limit;
        Debug.Log(gameObject.name+"---------------------------------"+ Vector3.Distance(startPos, transform.localPosition));

        return Mathf.Clamp(value, -1f, 1f);
    }

    private void Pressed()
    {
        isPressed = true;
        onPressed.Invoke();
        Debug.Log("Pressed");
    }

    private void Released()
    {
        isPressed = false;
        onReleased.Invoke();
        Debug.Log("Released");

       
    }
}
