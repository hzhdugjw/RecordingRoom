using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;

public class PushSwitch : MonoBehaviour
{
    public void OpenSwitch() {
        Animator animator = GetComponent<Animator>();
        animator.SetTrigger("PushSwitch");
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(audioSource.clip);
        GameObject[] loseGameObjectList = GameObject.FindGameObjectsWithTag("MeshLose");
        GameObject[] showGameObjectList = GameObject.FindGameObjectsWithTag("RunShow");
     
            foreach (GameObject gameObject in loseGameObjectList)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = false;
            }

            foreach (GameObject gameObject in showGameObjectList)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
        

    }

    public void CloseSwitch()
    {
        Animator animator = GetComponent<Animator>();
        animator.SetTrigger("PushSwitch");
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(audioSource.clip);
        GameObject[] loseGameObjectList = GameObject.FindGameObjectsWithTag("MeshLose");
        GameObject[] showGameObjectList = GameObject.FindGameObjectsWithTag("RunShow");
        foreach (GameObject gameObject in loseGameObjectList)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        }

        foreach (GameObject gameObject in showGameObjectList)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }


    }


    public void openRecorderSwitch()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(audioSource.clip);
        MusicManager.instance.setStateOfMusicManager(true);
    }

}
