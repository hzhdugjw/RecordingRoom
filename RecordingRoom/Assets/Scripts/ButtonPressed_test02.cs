using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ButtonPressed_test02 : MonoBehaviour
{
    public ConfigurableJoint joint;
    public float speed = 5f;
    public float maxAngle = 45f;

    private bool isPressed = false;
    private float targetAngle = 0f;

    void Start()
    {
        if (joint == null)
        {
            joint = GetComponent<ConfigurableJoint>();
        }
    }

    void Update()
    {
        if (isPressed)
        {
            float angle = joint.targetRotation.eulerAngles.z;
            float delta = Mathf.Clamp(targetAngle - angle, -speed, speed);
            joint.targetRotation *= Quaternion.Euler(0f, 0f, delta);
        }
    }

    public void OnButtonPress(XRBaseInteractor interactor)
    {
        isPressed = true;
        targetAngle = -maxAngle;
    }

    public void OnButtonRelease(XRBaseInteractor interactor)
    {
        isPressed = false;
        targetAngle = 0f;
    }
}