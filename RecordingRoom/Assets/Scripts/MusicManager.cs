using System;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    //public static List<String> instrumentList;
    public bool isOpenRecorder=false;
    public GameObject prefeb;
    public Transform initPoint;
    public static MusicManager instance;

    private void Start()
    {
        instance= this;
    }

    //录音机开始录音
    public void OpenRecorder()
    {
        if (isOpenRecorder == false)
        {
            //instrumentList = new List<String>();
            isOpenRecorder = true;

            Debug.Log("recorder has been opened now");
        }else {
            Debug.Log("recorder has already been opened");
        }
        
    }

    //录音机结束录音
    public void CloseRecorder()
    {
        if (isOpenRecorder == true)
        {
            isOpenRecorder = false;
            Debug.Log("recorder has been closed now");
        }
        else
        {
            Debug.Log("recorder has not been opened");
        }

    }

    public void initMusicBox(AudioClip clip) {
        GameObject clone = Instantiate(prefeb);
        clone.GetComponent<GrabObjectAttributes>().audioClip = clip;
        Debug.Log("aaaa");
    }

    public bool getStateOfMusicManager() { 
        return isOpenRecorder;
    }

    public void setStateOfMusicManager(bool state)
    {
        isOpenRecorder = state;
    }
    /*    //录音机在录音时敲击乐器录音
        public void PlayInstrument() {
            if (isOpenRecorder == true)
            {
                GameObject instrument  = this.gameObject;
                AudioSource instumentAudioSouce = instrument.GetComponent<AudioSource>();
                GameObject record=Instantiate(prefeb);
                Debug.Log(instumentAudioSouce.clip);
                record.GetComponent<AudioSource>().clip = instumentAudioSouce.clip;
                //instrumentList.Add(gameObject.name);
            }
        }*/

}
