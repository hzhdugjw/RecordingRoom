using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioPeer : MonoBehaviour
{
    AudioSource currentAudio;
    public static float[] samples = new float[512];
    public float[] freqBand = new float[8];
    public float[] bandBuffer = new float[8];
    float[] bufferDecrease = new float[8];
    // Start is called before the first frame update
    void Start()
    {
        currentAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentAudio.clip != null)
        {
            //Debug.Log("currentAudio:" + gameObject.transform.parent.gameObject.name + ":" + currentAudio.clip);
            GetSpectrumAudioSource();
            MakeFrequencyBands();
            BandBuffer();
        }
    }

    void GetSpectrumAudioSource()
    {
        currentAudio.GetSpectrumData(samples, 0, FFTWindow.Blackman);
    }

    //�ѷ�����ݲ����ʷ�Ϊ8��
    void MakeFrequencyBands()
    {
        //22050hz�����ʣ�22050/512=43 hz per sample
        int count = 0;
        for (int i = 0; i < 8; i++)
        {
            float average = 0;
            int sampleCount = (int)Mathf.Pow(2, i) * 2;
            if (i == 7)
            {
                sampleCount += 2;
            }
            for(int j = 0; j < sampleCount; j++)
            {
                average += samples[count] * (count + 1);
                count++;
            }

            average /= count;
            freqBand[i]= average*10;
        }
    }

    void BandBuffer()
    {
        for (int i = 0; i < 8; i++)
        {
            if (freqBand[i] > bandBuffer[i])
            {
                bandBuffer[i] = freqBand[i];
                bufferDecrease[i] = 0.005f;
            }
            if(freqBand[i] < bandBuffer[i])
            {
                bandBuffer[i] -= bufferDecrease[i];
                bufferDecrease[i] *= 1.2f;
            }
        }
    }
}
