using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public AudioClip clip; // The audio wanted to be played
    private AudioSource source; // The source of the audio

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        source.clip = clip;
    }

    public void PlaySound()
    {
        source.volume = 1;
        source.Play();
        StopAllCoroutines();
    }
    
    public void StopSound()
    {
        StartCoroutine(DecreaseVolume());
    }



    IEnumerator DecreaseVolume()
    {
        while (source.volume > 0.01)
        {
            source.volume -= 0.02f;
            yield return null;
        }
    }
}
