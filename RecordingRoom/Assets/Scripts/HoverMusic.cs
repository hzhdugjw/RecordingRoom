using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.Interaction.Toolkit;
/*public class AudioEventArgs : EventArgs
{
    public AudioSource audioSource { get; set; }
    public int Id { get; set; }
}

public delegate void AudioEventHandler(HoverMusic sender, AudioEventArgs e);*/

public class HoverMusic : MonoBehaviour
{
    //Id
    public int Id { get; }
    //move
    public float upDownSpeed;
    public float angel =180f;
    private float y;
    public float rotateSpeed = 3.0f;
    bool isCenter = false;
    //audio
    AudioSource audioSource;
    bool isPlaying = false;


    private void Start()
    {
        audioSource=GetComponentInParent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {
        other.attachedRigidbody.velocity = Vector3.zero;
        audioSource.clip = other.GetComponent<GrabObjectAttributes>().audioClip;
        //Change Layer
        other.gameObject.layer = 7;
        gameObject.layer = 7;

        other.attachedRigidbody.useGravity= false;

        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<GrabObjectAttributes>() != null && !other.GetComponent<GrabObjectAttributes>().isSelected)
        {
            movementOfMusicBox(other);
        }
        //Test music
/*        if (!isPlaying)
        {
            playMusic();
        }*/
    }

    private void OnTriggerExit(Collider other)
    {
        //stopMusic();

        audioSource.clip = null;
        isCenter = false;
        other.gameObject.layer = 0;
        gameObject.layer = 0;

    }

    public void movementOfMusicBox(Collider other) {

            other.gameObject.transform.Rotate(rotateSpeed, rotateSpeed, rotateSpeed, Space.World);



            if (isCenter == false)
            {
                if (Mathf.Approximately(other.transform.position.x, transform.position.x) && Mathf.Approximately(other.transform.position.z, transform.position.z) && Mathf.Approximately(other.transform.position.y, transform.position.y))
                {
                    isCenter = true;


                }
                else
                {
                    other.transform.position = Vector3.MoveTowards(other.transform.position, transform.position, 1 * Time.deltaTime);

                }
            }
            else
            {
                y += Time.deltaTime * angel;
                other.gameObject.transform.position += new Vector3(0, math.sin(y * (math.PI / 180)) / upDownSpeed, 0);
            }


        
    }
    public void playMusic() {
        audioSource.Play();
        audioSource.loop = true;
        isPlaying = true;
    }
    public void stopMusic() {
        isPlaying = false;
        audioSource.Stop();
        audioSource.loop = false;
        audioSource = null;
    }
}
