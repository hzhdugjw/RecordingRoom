using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour
{

    public AudioClip clip; // The audio wanted to be played
    private AudioSource source; // The source of the audio
    //public string targetTag; // To make sure only the object with the specifique tag can trigger the audio 
    public bool useVelocity = true;
    public float minVelocity = 0;
    public float maxVelocity = 2;


    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collider)
    {
        //if (collider.gameObject.tag == targetTag)
        //{
            VelocityEstimator estimator = collider.gameObject.GetComponent<VelocityEstimator>();
            if (estimator && useVelocity)
            {
                float v = estimator.GetVelocityEstimate().magnitude;
                float volume = Mathf.InverseLerp(minVelocity, maxVelocity, v);

                source.PlayOneShot(clip, volume);
            }
            else
            {
                source.PlayOneShot(clip);
            }
        Debug.Log("XIAOYE");

        //}

        if (MusicManager.instance.getStateOfMusicManager()) {
            MusicManager.instance.setStateOfMusicManager(false);
            MusicManager.instance.initMusicBox(clip);
            Debug.Log("CAO");

        }

    }
}
