using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamCube : MonoBehaviour
{
    public int band;
    public float startScale;
    public float scaleMultiplier;
    public AudioPeer audioPeer;

    //�Ƿ�ʹ��buffer
    public bool useBuffer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {       

        if (useBuffer)
        {
            //ͨ��bandBuffer�ı䷽��ĸ߶�
            transform.localScale = new Vector3(transform.localScale.x, (audioPeer.bandBuffer[band] * scaleMultiplier) + startScale, transform.localScale.z);
        }
        if (!useBuffer)
        {
            //�ı䷽��ĸ߶�
            transform.localScale = new Vector3(transform.localScale.x, (audioPeer.freqBand[band] * scaleMultiplier) + startScale, transform.localScale.z);
        }
    }
}
